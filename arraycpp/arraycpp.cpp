﻿#include <iostream>
#include <chrono>
#include <ctime>

int main()
{
    struct tm buf;

    auto now = time(0);

    auto gmtm = gmtime_s(&buf, &now);

    const int size = 20;

    const int TODAY = buf.tm_mday;

    int sum = 0;

    int array[size][size];

    for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
            array[i][j] = i + j;
            std::cout << array[i][j];
        }
        std::cout << '\n';
    }

    for (int i = 0; i < size; i++) {
        sum += array[TODAY / 20][i];
    }

    std::cout << sum;
}